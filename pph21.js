const pph21 = (salary, code) => {
    const rules = [
        [50000000, 0.05],
        [250000000, 0.15],
        [500000000, 0.25],
        [salary*12+1, 0.30]
    ]
    let taxable = salary * 12 - ptkp(code)
    if(taxable < 0) return 0
    let tax = 0; 
    let threshold = 0;
    let thray = []
    for(let i = 0; i<rules.length; i++){
        if(taxable > rules[i][0]){
            
            threshold = rules[i][0] - thray.reduce((a,b) => a+b, 0)
         
            taxable = taxable - threshold
            thray.push(threshold)
            
            tax += threshold * rules[i][1]
            
            
        } else{
            tax += taxable * rules[i][1]
            break;
        }
    }
    
    return tax
} 

const toIDR= (num) => {
    return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(num)
}

const ptkp = (code) => {
    const rules = [
        ['TK0', 54000000],
        ['K0', 58500000],
        ['K1', 63000000],
        ['K2', 67500000],
        ['K3', 72000000]
    ]

    relieve = rules.find(rule => rule[0] == code)
    if(relieve)
        return relieve[1]
    else
        return null
}

module.exports = {pph21, ptkp, toIDR}