const {pph21, ptkp} = require('./pph21')

describe('count tax', () => {
    it('should not return minus value', () => {
        expect(pph21(-1, 'TK0')).toBeGreaterThan(-1)
    })

    it('should return zero when salary < tax relieve', () => {
        expect(pph21(4500000, 'TK0')).toEqual(0)
    })

    it('should return more than 1 when salary > tax relieve', () => {
        expect(pph21(5000000, 'TK0')).toBeGreaterThan(1)
    })

    it('should only return a number', () => {
        expect(typeof(pph21(5000000, 'TK0'))).toBe('number')
    })

    it('should return zero', () => {
        expect(pph21(4500000, 'TK0')).toEqual(0)
    })

    it('should return zero', () => {
        expect(pph21(5000000, 'K1')).toEqual(0)
    })

    it('should return exactly 1723400000', () => {
        expect(pph21(500000000, 'K3')).toEqual(1723400000)
    })
    
})

describe('check tax relieve', () => {
    it('should return number when ptkp code match', () => {
        expect(typeof(ptkp('TK0'))).toBe('number')
    })
    it('should return null when ptkp code not found', () => {
        expect(ptkp('TKx0')).toBeNull()
    })
})
