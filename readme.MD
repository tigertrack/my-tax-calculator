to start the program, run:
```
npm run start
```

to initiate the testing suite, first install the required dependecy by running:
```
npm ci
```

and then begin the test by running:
```
npm run test
```