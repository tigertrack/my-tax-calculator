const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const {pph21, toIDR} = require('./pph21')

rl.question("Berapa salary yang kamu terima dalam 1 bulan ? ", function(salary) {
    rl.question("masukan jumlah tanggungan (isi dengan 0 jika belum berkeluarga) ", function(tanggungan) {
        if(isNaN(parseInt(salary)) || isNaN(parseInt(tanggungan)) || parseInt(salary) < 0 || parseInt(tanggungan) < 0) {
            console.log("jumlah salary & tanggungan harus diinput dengan format bilangan Ril")
            process.exit()
        }
        let ptkp = `${(tanggungan > 0) ? 'K' : 'TK'}${tanggungan}`
        console.log(`Jumlah pajak yang harus kamu keluarkan adalah senilai: ${toIDR(pph21(salary, ptkp))}`)
        rl.close();
            
    });
});
